# articles-techteek



## Getting started

To run the application please follow these steps:

- [ ] Clone the repo to your local.
- [ ] Run the following commands inside the project folder:

```
composer install
npm install
cp .env.example .env
php artisan key:generate
```
- [ ] Create a schema in your mysql database and call it "techteek"
- [ ] Change the .env db configuration to fit with yours (username, password)
- [ ] Run the follwing commands to create the database tables.

```
php artisan migrate
```
- [ ] Now to run the app use these commands:

```
php artisan serve
npm run dev
```

Now test the application [here](http://localhost:8000/login).
