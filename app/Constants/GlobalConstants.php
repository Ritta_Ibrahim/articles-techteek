<?php

namespace App\Constants;

class GlobalConstants
{
    const PAGE_SIZE_SMALL = 10;
    const PAGE_SIZE_MEDIUM = 50;
    const PAGE_SIZE_BIG = 100;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    public function getStatus(){
        return [
            self::STATUS_ACTIVE => "active",
            self::STATUS_INACTIVE => "inactive",
        ];
    }

}
