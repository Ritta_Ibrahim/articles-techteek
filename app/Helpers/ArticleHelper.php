<?php

namespace App\Helpers;

class ArticleHelper
{
    public static function createSlug($title)
    {
        return str_replace(' ', '-', $title);
    }
}
