<?php

namespace App\Http\Controllers;

use App\Constants\GlobalConstants;
use App\Models\Article;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('home.index', [
            'articles' => Article::latest()->filter(request(['search']))->paginate(GlobalConstants::PAGE_SIZE_MEDIUM),
        ]);
    }

    public function show($slug)
    {
        $title = str_replace('-', ' ', $slug);
        $article = Article::findByTitle($title);

        return view('home.show', compact('article'));
    }
}
