<?php

namespace App\Models;

use App\Helpers\ArticleHelper;
use App\Traits\CreatedUpdatedBy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory, CreatedUpdatedBy;

    protected $guarded = [];

    public function scopeFilter($query, array $filters)
    {
        if (isset($filters['search']) && $filters['search'] != "") {
            $query->where('title', 'like', '%' . $filters['search'] . '%')
                ->orWhere('content', 'like', '%' . $filters['search'] . '%')
            ;
        }
    }

    public static function findByTitle($title)
    {
        return self::where('title', 'like', '%' . $title . '%')->first();
    }

    public function getSlug(){
        return ArticleHelper::createSlug($this->title);
    }
}
