<?php
return [
    'create_article' => 'Create New Article',
    'title' => 'Title',
    'content' => 'Content',
    'created_at' => 'Created at',
    'actions' => 'Actions',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'articles' => 'Articles',
    'website' => 'Go to the website'
];
