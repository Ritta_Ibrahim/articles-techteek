oldData = $('input#content').val();
blocks = (oldData == "" || oldData == undefined || oldData == null) ? {} : JSON.parse(oldData);

var editor = new EditorJS({
    holderId: 'editorjs',

    tools: {
        gif: {
            class: Gif,
        },
    },

    data: blocks,

    initialBlock: 'paragraph',
});

const form = $('#article-form');

form.submit(function () {
    editor.save().then(savedData => {
        $('#content').val(JSON.stringify(savedData));
    });
    
});