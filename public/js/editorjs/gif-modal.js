$('#gif-form').submit(function (event) {
    event.preventDefault();
    var formAction = $(this).attr("action");
    var method = $(this).attr("method");
    var formData = $(this).serialize();

    $.ajax({
        url: formAction,
        type: method,
        data: formData
    }).done(function (response) {
        $(".results").html("");
        let res = "";
        response.forEach((link, index) => {
            res += '<div class="card col-2 m-4"><input type="checkbox" id="gif' + index +
                '"  name="gif' + index +
                '"  value=' + link +
                '/><label for="gif' + index +
                '"><img height="100px" width="100px" src=' + link +
                '"></label></div>';
        });
        $(".results").html(res);
    });
});

$('#results').submit(async function (event) {
    event.preventDefault();

    var formData = $(this).serialize();

    blockIndex = -1;
    urlObjs = formData.split('&');

    urls = [];
    var i = 0;
    urlObjs.forEach((obj, index) => {
        if (index == 0) {
            blockIndex = obj.split('=')[1];
            i = blockIndex;
        } else {
            i++;
            u = decodeURIComponent(obj.split('=')[1]);
            editor.blocks.insert("gif", { url: u }, {}, i);
        }
    });

    await editor.save();

    $('#block-index').val("");
    $('#exampleModal').modal('hide');
});