class Gif {
  static get isReadOnlySupported() {
    return true
  }

  static get toolbox() {
    return {
      title: 'Gif',
      icon: '<svg width="17" height="15" viewBox="0 0 336 276" xmlns="http://www.w3.org/2000/svg"><path d="M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z"/></svg>'
    };
  }

  constructor({ data, api }) {
    this.data = data;
    this.api = api;
  }

  render() {
    this.wrapper = document.createElement('div');
    const img = document.createElement('img');
    img.src = this.data && this.data.url ? this.data.url : '/gif.jpg';

    var i = this.api.blocks.getBlocksCount();
    img.addEventListener('click', function () {
      $('#block-index').val(i);
      $('#exampleModal').modal('show');
    });

    this.wrapper.classList.add('container');
    this.wrapper.classList.add('justify-content-center');
    this.wrapper.classList.add('m-3');
    this.wrapper.appendChild(img);

    return this.wrapper;
  }

  save(blockContent) {
    const image = blockContent.querySelector('img');

    if (!image) {
      return this.data;
    }

    return Object.assign(this.data, {
      url: image.src
    });
  }

  renderSettings() {
    var i = this.api.blocks.getCurrentBlockIndex();
    return [
      {
        icon: '<svg width="17" height="15" viewBox="0 0 336 276" xmlns="http://www.w3.org/2000/svg"><path d="M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z"/></svg>',
        label: 'Change gif',
        onActivate: () => {
          $('#block-index').val(i);
          $('#exampleModal').modal('show');
        }
      }
    ]
  }

}