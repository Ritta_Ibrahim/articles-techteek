<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('messages.articles') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="row justify-content-center">
                        <div class="col-9  p-5">
                            <div class="card shadow  d-flex justify-content-center align-items-center">
                                <div class="card-title d-flex justify-content-center align-items-center">
                                    <h2>{{ __('messages.create_article') }}</h2>
                                </div>
                                <hr>
                                <div class="card-body w-100">
                                    <form id="article-form" action="{{ route('articles.update', $article->id) }}"
                                        method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="title"
                                                    class="col-form-label">{{ __('messages.title') }}</label>
                                                <input type="text" name="title" value="{{ $article->title }}"
                                                    class="form-control" placeholder="Title" require>
                                                @error('title')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>

                                        <br>
                                        <div class="form-group">
                                            <label for="content">{{ __('messages.content') }}</label>
                                            <input name="content" type="hidden" class="form-control" id="content"
                                                value="{{ $article->content }}">
                                            <div id="editorjs"></div>
                                        </div>

                                        <div class="form-group w-100 d-flex justify-content-center align-items-center">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <x-admin.modal></x-admin.modal>

                </div>
            </div>
        </div>
    </div>

</x-app-layout>
