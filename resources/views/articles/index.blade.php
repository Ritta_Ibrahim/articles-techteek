<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('messages.articles') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <a class="btn btn-primary"
                        href="{{ route('articles.create') }}">{{ __('messages.create_article') }}</a>
                    <table class="table">
                        <thead>
                            <tr class="">
                                <th scope="col">#</th>
                                <th scope="col">{{ __('messages.title') }}</th>
                                <th scope="col">{{ __('messages.created_at') }}</th>
                                <th scope="col">{{ __('messages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($articles as $article)
                                <tr>
                                    <th scope="row">{{ $article->id }}</th>
                                    <td>{{ $article->title }}</td>
                                    <td>{{ $article->created_at }}</td>
                                    <td>
                                        <form action="{{ route('articles.destroy', $article) }}" method="POST">
                                            <a class="btn btn-primary m-1"
                                                href="{{ route('articles.edit', $article) }}">{{ __('messages.edit') }}</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit"
                                                class="btn btn-danger">{{ __('messages.delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $articles->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
