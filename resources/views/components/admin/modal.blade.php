<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-light">
            <div class="modal-title row m-2 align-items-center justify-content-center">
                <div class="col-4">
                    <form action="http://localhost:3000/api/gifs" method="GET" id="gif-form">
                        <div class="mb-3">
                            <input type="text" name="q" class="form-control" id="search-input"
                                placeholder="Search GIF ..">
                            <input type="submit" style="visibility: hidden" />
                        </div>
                    </form>
                </div>
            </div>
            <form id="results">
                <input type="hidden" name="index" class="form-control" id="block-index">
                <div class="modal-body row align-items-center justify-content-center">
                    <div class="col-12">
                        <div class="container">
                            <div class="row results">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="insert" class="btn btn-primary">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>
