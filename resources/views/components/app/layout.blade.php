<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Articles</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>

    <div class="container-fluid p-0 m-0">
        <x-app.navbar></x-app.navbar>
    </div>

    <div class="container-fluid p-0 m-0">
        {{ $slot }}
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/@editorjs/editorjs@latest"></script>
    <script src="{{ asset('js/editorjs/plugins/gif/gif.js') }}"></script>
    <script src="{{ asset('js/editorjs/plugins/gif/gif.css') }}"></script>

    <script>
        var editorContent = $('#editor-content').val();
        blocks = (editorContent == "" || editorContent == undefined || editorContent == null) ? {} : JSON.parse(
            editorContent);

        var readOnlyEditor = new EditorJS({
            holderId: 'read-only-editor',

            tools: {
                gif: {
                    class: Gif,
                },
            },

            data: blocks,

            initialBlock: 'paragraph',

            readOnly: true,
        });
    </script>

</body>

</html>
