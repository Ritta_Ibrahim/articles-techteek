<x-app.layout>
    <div class="container">
        <div class="row p-2 w-100 justify-content-center">
            @foreach ($articles as $article)
                <a class=" col-9 article-link" href="/article/{{ $article->getSlug() }}">
                    <div class="card article-title shadow">
                        {{ $article->title }}
                    </div>
                </a>
            @endforeach
        </div>

        {{ $articles->links() }}
    </div>
</x-app.layout>
