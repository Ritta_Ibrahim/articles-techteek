<x-app.layout>
    <div class="container">
        <div class="row p-2 w-100 justify-content-center">
            <h1 class="text-center">{{ $article->title }}</h1>
            <input id="editor-content" type="hidden" value="{{ $article->content }}">
            <div id="read-only-editor"></div>
        </div>
    </div>
</x-app.layout>
